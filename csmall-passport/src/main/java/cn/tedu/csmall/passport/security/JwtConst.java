package cn.tedu.csmall.passport.security;

public interface JwtConst {

    String KEY_USERNAME = "username";
    String KEY_PERMISSIONS = "permissions";

}
